var app = angular.module("myApp", ['flux']);

app.config(function (fluxProvider) {
    fluxProvider.setImmutableDefaults({ immutable: false });
});

app.store('MyStore', function () {
    return {
        initialize: function () {
            this.state = this.immutable({
                person: {
                    name: 'Jane',
                    age: 30,
                    likes: 'awesome stuff',
                },
            })
        },
        handlers: {
            SAVE_PERSON: 'savePerson',
            SAVE_NAME: 'saveName'
        },
        savePerson: function (payload) {
            this.state.merge('person', payload.person)
        },
        saveName: function (payload) {
            this.state.set(['person', 'name'], payload.name)
        },
        exports: {
            get person() {
                return this.state.get('person')
            },
        },
    }
});

app.component('myComponent', {
    restrict: 'E',
    template: '<div>Component: {{ vm.person }}, {{ vm.name }}</div>',
    controllerAs: 'vm',
    controller: function (MyStore, myStoreActions, flux, $timeout) {
        var vm = this;
        vm.savePerson = myStoreActions.savePerson;
        flux.listenTo(MyStore, setStoreVars);
        flux.listenTo(MyStore, ['person', 'name'], setName);

        // One need to explicitly call, callbacks if initialization is expected to happen
        // As flux.listenTo does not calls it explicitly during initiliaztion while in $scope.$listenTo
        // does that for you.

        setStoreVars();
        setName();

        function setStoreVars() {
            vm.person = MyStore.person;
        }

        function setName() {
            vm.name = MyStore.person.name;
            console.log("Component Person name is now: " + vm.name);
        }

        $timeout(function(){
            flux.dispatch('SAVE_NAME', { name: 'Bob' });
        }, 3000);
    },
});

app.service('myStoreActions', function (flux) {
    var service = {
        savePerson: savePerson,
    }

    return service

    function savePerson(person) {
        flux.dispatch('SAVE_PERSON', { person: person });
    }
});

app.directive('myDirective', function () {
    return {
        restrict: 'E',
        template: '<div>Directive: {{ vm.person }}, {{ vm.name }}</div>',
        controllerAs: 'vm',
        controller: function ($scope, MyStore, myStoreActions, $timeout, flux) {
            var vm = this;
            vm.savePerson = myStoreActions.savePerson;
            $scope.$listenTo(MyStore, setStoreVars);
            $scope.$listenTo(MyStore, ['person', 'name'], setName);

            function setStoreVars() {
                vm.person = MyStore.person;
            }

            function setName() {
                vm.name = MyStore.person.name;
                console.log("Directive Person name is now: " + vm.name);
            }

            $timeout(function(){
                flux.dispatch('SAVE_NAME', { name: 'Alice' });
            }, 6000);
        },
    }
});